/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlleur;
import classe.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import modele.*;
import classe.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author itu
 */

public class FrontServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
           ServletContext servletContext= request.getServletContext();
           String maka_url= request.getRequestURI();
           out.println(maka_url);
           Utilitaire u=new Utilitaire();
           out.println(u.retrieveUrlFromRawurl(maka_url));
           HashMap<String, HashMap<Class,Method> > urlSave= (HashMap<String, HashMap<Class, Method>>) servletContext.getAttribute("url");
           InvocMethod(urlSave,u.retrieveUrlFromRawurl(maka_url),request,response);
//           
           
           
           
           
           
           
           
//      TEST POUR SPRINT 2     
//           
//                String path = this.getServletContext().getRealPath("/")+"WEB-INF/classes";
//        File currentDir = new File(path);
//
//        ArrayList<Class> t = new ArrayList<>();
//        Utilitaire f = new Utilitaire();
//      try {
//            f.GetAllClasses(currentDir,t);
//        } catch (ClassNotFoundException e) {
//            throw new RuntimeException(e);
//        }
//        ServletContext context=this.getServletContext();
//        if (context.getAttribute("url")==null){
////         
//            HashMap<String, HashMap<Class,Method> > urlSaves=f.ConvertToHashMapMethod(t);
//            context.setAttribute("url",urlSaves);
//            for (Map.Entry<String, HashMap<Class, Method>> set : urlSaves.entrySet()) {
//           for (Map.Entry<Class,Method> sets : set.getValue().entrySet()) {
//           out.println("Classe:"+sets.getKey().getName() +"Method:"+sets.getValue().getName());
//        }
//        }
//        }
           
        }
    }
    
    
    
//SPRINT 3
    public void InvocMethod(HashMap<String, HashMap<Class, Method>> method,String url,HttpServletRequest req,HttpServletResponse res) throws Exception {
        new Utilitaire().CheckUrl(url, method);

        HashMap<Class, Method> m = method.get(url);
        Method f = null;
        Class cls = null;
        for (Map.Entry<Class, Method> set : m.entrySet()) {
            f = set.getValue();
            cls = set.getKey();
        }
        String[] listInput = new Utilitaire().getFieldValids(cls);
        String[] value = new String[listInput.length];
        for (int i = 0; i < listInput.length; i++) {
            value[i] = req.getParameter(listInput[i]);
        }
            Utilitaire u = new Utilitaire();
        Object o = cls.newInstance();
            u.InvokeMethodSet(listInput,o,value);

        ModelView view = (ModelView) f.invoke(o);

        req.setAttribute(view.getAttribute(), view.getValue());
        RequestDispatcher dist = req.getRequestDispatcher(view.getUrlPage());
        dist.forward(req, res);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();

        try {
            processRequest(req, resp);
        } catch (Exception e) {
            out.println("<br>"+e.getMessage());
           e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(FrontServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
//COMMENTER POUR TESTER SPRINT 2
    @Override
    public void init() {
        String path = this.getServletContext().getRealPath("/")+"WEB-INF/classes";
        File currentDir = new File(path);

        ArrayList<Class> t = new ArrayList<>();
        Utilitaire f = new Utilitaire();
        try {
            f.GetAllClasses(currentDir,t);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        ServletContext context=this.getServletContext();
        if (context.getAttribute("url")==null){
       
            HashMap<String, HashMap<Class,Method> > urlSave=f.ConvertToHashMapMethod(t);
            context.setAttribute("url",urlSave);
            
        }
    }


}
