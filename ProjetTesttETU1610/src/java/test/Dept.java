package test;


import annoter.*;
import modele.ModelView;

import java.util.HashMap;

public class Dept {
//    attributes4
    @Formulaire
    private int id;
    @Formulaire
    private String dept;
//methodes
    public String getDept() {
        return dept;
    }
    public void setDept(String dept) {
        this.dept = dept;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
//eto ny methode mireturn modelview
    @UrlMethodAnnotation(urlMethodAnnotation = "dept-essai")
    public ModelView display(){
        ModelView mod = new ModelView();
        mod.setAttribute("text");
        mod.setUrlPage("Reponse.jsp");
        mod.setValue("Here we can see that value dosn't exist");
        return mod;
    }
    @UrlMethodAnnotation(urlMethodAnnotation = "dept-info")
    public ModelView displayInfo(){
        ModelView mod = new ModelView();
        mod.setAttribute("text");
        mod.setUrlPage("Reponse.jsp");
        mod.setValue("Avec l'id="+getId()+"<br> possède le nom de département ="+getDept());
        return mod;
    }
}
